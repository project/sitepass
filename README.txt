*************************************************************
SitePass - No Password Authentication

Copyright 2006 by D R Pratten <http://www.davidpratten.com>
Distributed under the GNU General Public Licence
*************************************************************

PURPOSE
=======
Site Pass is a module that enables user authentication without passwords.

When using the Site Pass Module, users who wish to login to your site enter by using two steps.  They enter their email address into the "Site Pass Request" block.  Then they are sent a Site Pass Number by email. Users may either enter the Site Pass Number to login or click on the URL in the email.

A Site Pass is a 6 digit code (like "G5FRD7") which is valid for 24hours, one time only, from a computer at the IP address that requested it. After 3 failed login attempts from an IP address, all unused Site Passes for that IP address will be deleted.

INSTALLATION
============
1. Put the module in your modules/ directory, or your sites/all/ directory.
2. Visit Administer -> Site Building -> Modules and enable the Site Pass module.
3. Visit Administer -> User management -> Site Passes and customise welcome emails.  Do not show the user's password in these emails! 
4. Visit Administer -> Site Building -> Blocks and enable the "Site Pass Request" and "Site Pass User Login" blocks; Optionally disable the "User login" block.

UPDATE SITEPASS
===============
1. Download the new version of Sitepass.
2. Login in to your site as an administrator.
3. Put the module in your modules/ directory, or your sites/all/ directory.
4. Visit www.yoursite.com/updates.php and follow instructions to update your site.

DISABLE
=======
1. Visit Administer / Site Building / Modules and disable the Site Pass module.

UNINSTALL
=========
Drupal does not currently support a clear way of uninstalling modules.  If you wish to completely remove all parts of Site Pass Module then:
1. Backup your database (in case things go pear shaped).
2. Visit Administer / Site Building / Modules and disable the Site Pass module.
3. Delete the sitepass directory from under your modules/ directory
*** IN THE NEXT STEP, IF YOU DELETE THE WRONG RECORD FROM THE SYSTEM TABLE YOU CAN MAKE YOUR SITE UNUSABLE ***
4.  Use phpmyadmin or similar database tool to delete Site Pass Module's entry in the system file.  Site Pass Module's entry will have field filename = "modules/sitepass/sitepass.module" or similar.

CHANGE LOG
==========
See sitepass.install for Change Log.

FEATURES
========
- 100% Compatible with and no-overlap with core User module and Logintoboggan module.
- Response for a sitepass is different for unknown email addresses and for inactive ones.
- sitepasses will be automatically cleaned up for deleted users
- sitepasses for an ip address will be deleted after 3 failed attempts from that ip address
- sitepasses expire after 24hours from creation
- disables editting of the welcome letters via user.module
- Installation system - add automatic install of db table and auto upgrade of table
- on enable change the welcome letters to new defaults and saves old welcome emails into permanent storage if they were customised. not reveal passwords
- on disable of site pass and null out the modified user welcome messages.
- watchdog failed entries
- on enable change the welcome letters to not reveal passwords and direct to main url instead of login url
- cookie life-time setting
- note that if some of your users have a permanent password then use /user/login as a link it that will work for key people like greg
- enable two blocks, disable login block (when comfortable)

WISHLIST
========
- add CAPTCHA to request for Site pass.
- options - request sitepass with username

DATABASE
========
CREATE TABLE `sp_passes` (
  `passnumber` varchar(32) NOT NULL default '',
  `timestamp` int(11) NOT NULL default '0',
  `remote_addr` varchar(15) NOT NULL default '',
  `uid` int(11) NOT NULL default '0',
  `attempts` int(11) NOT NULL default '0',
  PRIMARY KEY  (`passnumber`),
  UNIQUE KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 