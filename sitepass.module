<?php
/**
 * @file
 * SitePass - No Password Authentication
 *
 * Copyright 2006 by D R Pratten <http://www.davidpratten.com>
 * Distributed under the GNU General Public Licence
 */

/**
 * Implementation of hook_menu().
 */
function sitepass_menu($may_cache) {
  global $user;
  $items = array();
  if (!$may_cache) {
    $items[] = array(
    'path' => 'admin/user/sitepass',
    'title' => t('Site Passes'),
    'description' => t('Set up custom options for Site Passes.'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('sitepass_main_settings')
    );
    
    $items[] = array('path' => 'sitepass', 'title' => t('Site Pass'),
    'callback' => 'drupal_get_form', 'callback arguments' => array('user_register'),
    'access' => !$user->uid, 'type' => MENU_CALLBACK);
    $items[] = array('path' => 'sitepass/login', 'title' => t('Login with Site Pass'),
    'callback' => 'drupal_get_form', 'callback arguments' => array('sitepass_login'), 'access' => !$user->uid, 'type' => MENU_LOCAL_TASK);
    $items[] = array('path' => 'sitepass/directlogin', 'title' => t('Login with Site Pass'),
    'callback' => 'drupal_get_form', 'callback arguments' => array('sitepass_direct_login'), 'access' => !$user->uid, 'type' => MENU_CALLBACK);
    $items[] = array('path' => 'sitepass/request', 'title' => t('Request Site Pass'),
    'callback' => 'drupal_get_form', 'callback arguments' => array('sitepass_request'), 'access' => !$user->uid, 'type' => MENU_LOCAL_TASK);
    $items[] = array('path' => 'sitepass/send', 'title' => t('Send Sitepass'),
      'callback' => 'drupal_get_form', 'callback arguments' => array('sitepass_send'), 'access' => !$user->uid, 'type' => MENU_CALLBACK);

    //DRP
    $items[] = array('path' => 'sitepass/register', 'title' => t('Create new account'),
    'callback' => 'drupal_get_form', 'callback arguments' => array('user_register'), 'access' => !$user->uid && variable_get('user_register', 1), 'type' => MENU_LOCAL_TASK);
  }
  return $items;
  
}

/**
 * Implementation of hook_block().
 */
function sitepass_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;
  // see if there are any Passes available for this ip address
  $result = db_query("SELECT remote_addr FROM {sp_passes} WHERE remote_addr='%s'", $_SERVER['REMOTE_ADDR']);
  $sitepass = db_fetch_object($result);
  $passpresent = !empty($sitepass->remote_addr);
  
  
  if ($op == 'list') {
    $blocks[0]['info'] = t('Site Pass Request');
    $blocks[1]['info'] = t('Site Pass User Login');
    return $blocks;
  }
  else if ($op == 'view') {
    $block = array();
    
    switch ($delta) {
      case 0:
        // if not logged and no sitepass available for this ip address at the present
        // For usability's sake, avoid showing two request forms on one page. 
        if (!$user->uid  && !$passpresent && !(arg(0) == 'sitepass')) {
          $block['subject'] = t('Request a Site Pass');
          $block['weight'] = 0;
          $block['content'] = drupal_get_form('sitepass_request');
        }
        return $block;
      
      case 1:
        // if not logged in and at least one site pass present for this ip
        // For usability's sake, avoid showing two login forms on one page. 
        if (!$user->uid && $passpresent  && !(arg(0) == 'sitepass')) {
        
          $block['subject'] = t('User Login');
          $block['weight'] = -1;
          $block['content'] = drupal_get_form('sitepass_login');
      }
      return $block;
    }
  }
}

function sitepass_request() {
  
  $form = array(
  '#id' => 'user-sitepass_request-form',
  '#base' => 'sitepass',
  );
  $form['mail'] = array('#type' => 'textfield',
  '#title' => t('User E-mail address'),
  '#maxlength' => 60,
  '#size' => 20,
  '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit',
  '#value' => t('E-mail Site Pass'),
  );
  $items = array();
  $items[] = l(t('Create new account'), 'sitepass/register', array('title' => t('Create a new user account.')));
  $form['links'] = array('#value' => theme('item_list', $items));
  
  return $form;
}

/**
 * Adapted from user.module user_pass_validate
 */
function sitepass_request_validate($form_id, $form_values) {
  $mail = $form_values['mail'];
  $account = user_load(array('mail' => $mail));
  if ($account->uid) {
    form_set_value(array('#parents' => array('account')), $account);
  }
}

/**
 * Copied from user.module user_pass_submit
 */
function sitepass_request_submit($form_id, $form_values) {
  global $base_url;
  
  $account = $form_values['account'];
  if (!$account->uid) {
    //form_set_error
    form_set_error('mail', t('Sorry, email address %mail is not known at this site. Please check and reenter, or create a new account for this email address..', array('%mail' => $form_values['mail'])));
    return "sitepass/request";
  }
  
  if (!$account->status==1) {
    form_set_error('mail', t('Sorry, email address %mail account is not active. If you have just registered a new account, we will be in touch with you as soon as your account is active.', array('%mail' => $form_values['mail'])));
    return "sitepass/request";
    
  }
  
  $from = variable_get('site_mail', ini_get('sendmail_from'));
  
  // Mail one time login URL and instructions.
  $passnumber = create_pass($account);
  $variables = array('!username' => $account->name, '!site' => variable_get('site_name', 'Drupal'), '!login_url' => sitepass_url($passnumber), '!uri' => $base_url, '!passnumber' => $passnumber, '!uri_brief' => substr($base_url, strlen('http://')), '!mailto' => $account->mail, '!date' => format_date(time()), '!login_uri' => url('user', NULL, NULL, TRUE), '!edit_uri' => url('user/'. $account->uid .'/edit', NULL, NULL, TRUE));
  $subject = _sitepass_mail_text('sitepass_subject', $variables);
  $body = _sitepass_mail_text('sitepass_body', $variables);
  $mail_success = drupal_mail('user-pass', $account->mail, $subject, $body, $from);
  
  if ($mail_success) {
    
    watchdog('user', t('Site Pass mailed to %name at %email. ', array('%name' => $account->name, '%email' => $account->mail)));
    drupal_set_message(t('A Site Pass has been sent to your e-mail address. Check your e-mail and follow the instructions to login from your current location.',$variables));
    return "sitepass/login";
  }
  else {
    watchdog('user', t('Error mailing Site Pass to %name at %email.', array('%name' => $account->name, '%email' => $account->mail)), WATCHDOG_ERROR);
    drupal_set_message(t('Unable to send Site Pass by mail. Please contact the site admin.'));
  }
  return;
  //return 'user';
}

function sitepass_url($passnumber) {
  return url("sitepass/directlogin/".$passnumber, NULL, NULL, TRUE);
}

function create_pass($account) {
  // Base 36 so that it is easy to say and copy.  Replace easy to confuse letters.
  // so that there is no confusion.  It ends up only a 33 letter alphabet.
  $passnumber = substr(_dec2string(_string2dec(substr(md5(time()),2,12),16),36),2,6);
  $passnumber = str_replace("0", "9", $passnumber);
  $passnumber = str_replace("O", "P", $passnumber);
  $passnumber = str_replace("1", "2", $passnumber);
  $passnumber = str_replace("I", "J", $passnumber);
  
  //echo $passnumber;
  //exit;
  // delete up earlier Site Passes for this user
  db_query("DELETE FROM {sp_passes} WHERE uid = '%d'", $account->uid);
  
  db_query("INSERT INTO {sp_passes} (passnumber, timestamp, remote_addr, uid) VALUES ('%s', '%d', '%s', %d)", md5($passnumber), time(), $_SERVER['REMOTE_ADDR'],$account->uid );
  return $passnumber;
}

function sitepass_login() {
  
  $form = array(
  '#id' => 'user-usesitepass-form',
  '#base' => 'sitepass',
  );
  $form['passnumber'] = array('#type' => 'textfield',
  '#title' => t('Site Pass Number'),
  '#maxlength' => 60,
  '#size' => 15,
  '#required' => TRUE,
  );
  $form['submit'] = array('#type' => 'submit',
  '#value' => t('Login'),
  );
  $items = array();
  $items[] = l(t('Create new account'), 'sitepass/register', array('title' => t('Create a new user account.')));
  $items[] = l(t('Request Site Pass'), 'sitepass/request', array('title' => t('Request a new Site Pass')));
  $form['links'] = array('#value' => theme('item_list', $items));
  
  return $form;
}

function sitepass_login_validate($form_id, $form_values) {
}

function sitepass_login_submit($form_id, $form_values) {
  return sitepass_do_login($form_values['passnumber']);
}

function sitepass_direct_login($passnumber) {
  drupal_goto(sitepass_do_login($passnumber));
}

/**
 * Copied from user.module user_pass_login
 * Menu callback; process one time login link and redirects to entry page.
 */
function sitepass_do_login($passnumber) {
  global $user;
  // Check if the user is already logged in. The back button is often the culprit here.
  if ($user->uid) {
    drupal_set_message(t('It is not necessary to use a Site Pass to login at this time. You are already logged in.'));
    return 'node'; /*drupal_goto();*/
  }
  else {
    // Clean up the Passes. Remove expired and ones for ip addresses with 3 or more failed attempts
    db_query("DELETE FROM {sp_passes} WHERE (remote_addr = '%s' and attempts >= %d) or timestamp<%d", $_SERVER['REMOTE_ADDR'], 3, time()-86400);
    // See if there is a Pass for this sitepass
    $result = db_query("SELECT passnumber, timestamp, remote_addr, uid FROM {sp_passes} WHERE passnumber='%s' and remote_addr='%s'",md5(strtoupper(trim($passnumber))),$_SERVER['REMOTE_ADDR']);
    $sitepass = db_fetch_object($result);
    if ($sitepass->uid) {
      // Clean up the Pass for this uid found
      db_query("DELETE FROM {sp_passes} WHERE uid = %d", $sitepass->uid);
      $account = user_load(array('uid' => $sitepass->uid, 'status' => 1));
      if ($account->uid) {
        
        watchdog('user', t('Session opened for %name.', array('%name' => $user->name)));
        $user = $account;
        
        // Update the user table timestamp noting user has logged in.
        db_query("UPDATE {users} SET login = '%d' WHERE uid = '%s'", time(), $user->uid);
        
        $edit = array();
        user_module_invoke('login', $edit, $user);
	$login_successful = variable_get('sitepass_login_successful', '');
	if ($login_successful!='') {
          drupal_set_message(t(variable_get('sitepass_login_successful', '')));
        }
        return('');
      }
      else {
        // Pass referred to now non-existant uid
        return note_attempt($passnumber);
      }
    }
    else {
      // Passnumber did not find a pass
      return note_attempt($passnumber);
    }
  }
}



function note_attempt($passnumber) {
  form_set_error('mail',t('Sorry, %passnumber is not a valid Site Pass Number. It may be mistyped, expired, or requested from another location. Check the number and try again, or request a new Site Pass.', array('%passnumber' => $passnumber)));
  // increment all the pass records for this ip address
  
  db_query("UPDATE {sp_passes} SET attempts = attempts+1 WHERE remote_addr = '%s'", $_SERVER['REMOTE_ADDR']);
  
  watchdog('user', t('Failed login attempt Site Pass %passnumber.', array('%passnumber' => $passnumber)), WATCHDOG_ERROR);

  return 'sitepass/login' ;// not necessary keep it simple.
}




/**
 * Implementation of hook_user().
 */
function sitepass_user($type, &$edit, &$user, $category = NULL) {
  // clean up site passes for deleted users
  if ($type == 'delete') {
    db_query("DELETE FROM {sp_passes} WHERE uid = %d", $user->uid);
  }
  
}

function sitepass_main_settings() {

  $form['general'] = array('#type' => 'fieldset', '#title' => t('General Settings'));
  $form['general']['sitepass_login_successful'] = array(
    '#type' => 'textarea',
    '#title' => t('Login Successful Message'),
    '#default_value' => variable_get('sitepass_login_successful', ''),
    '#description' => t('If entered here, users will receive a message upon login.'),
  );
  
  // User e-mail settings  adapted from user.module
  $form['email'] = array('#type' => 'fieldset', '#title' => t('User e-mail settings'));
  // Sitepass email
  $form['email']['user_mail_sitepass_subject'] = array('#type' => 'textfield', '#title' => t('Subject of Site Pass e-mail'),  '#default_value' => _sitepass_mail_text('sitepass_subject'), '#maxlength' => 180, '#description' => t('Customize the subject of your Site Pass e-mail, which is sent to new members upon request.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !date, !login_uri, !edit_uri, !login_url.'); //
  $form['email']['user_mail_sitepass_body'] = array('#type' => 'textarea', '#title' => t('Body of Site Pass e-mail'), '#default_value' => _sitepass_mail_text('sitepass_body'), '#rows' => 15, '#description' => t('Customize the body of the Site Pass e-mail, which is sent to new members upon request.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !login_uri, !edit_uri, !login_url.');
  
  $form['email']['user_mail_welcome_subject'] = array('#type' => 'textfield', '#title' => t('Subject of welcome e-mail'),  '#default_value' => _sitepass_mail_text('welcome_subject'), '#maxlength' => 180, '#description' => t('Customize the subject of your welcome e-mail, which is sent to new members upon registering.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !date, !login_uri, !edit_uri, !login_url.'); //
  $form['email']['user_mail_welcome_body'] = array('#type' => 'textarea', '#title' => t('Body of welcome e-mail'), '#default_value' => _sitepass_mail_text('welcome_body'), '#rows' => 15, '#description' => t('Customize the body of the welcome e-mail, which is sent to new members upon registering.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !login_uri, !edit_uri, !login_url.');
  $form['email']['user_mail_admin_subject'] = array('#type' => 'textfield', '#title' => t('Subject of welcome e-mail (user created by administrator)'), '#default_value' => _sitepass_mail_text('admin_subject'), '#maxlength' => 180, '#description' => t('Customize the subject of your welcome e-mail, which is sent to new member accounts created by an administrator.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !date, !login_uri, !edit_uri, !login_url.');
  $form['email']['user_mail_admin_body'] = array('#type' => 'textarea', '#title' => t('Body of welcome e-mail (user created by administrator)'), '#default_value' => _sitepass_mail_text('admin_body'), '#rows' => 15, '#description' => t('Customize the body of the welcome e-mail, which is sent to new member accounts created by an administrator.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !login_uri, !edit_uri, !login_url.');
  $form['email']['user_mail_approval_subject'] = array('#type' => 'textfield', '#title' => t('Subject of welcome e-mail (awaiting admin approval)'), '#default_value' => _sitepass_mail_text('approval_subject'), '#maxlength' => 180, '#description' => t('Customize the subject of your awaiting approval welcome e-mail, which is sent to new members upon registering.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !date, !login_uri, !edit_uri, !login_url.');
  $form['email']['user_mail_approval_body'] = array('#type' => 'textarea', '#title' => t('Body of welcome e-mail (awaiting admin approval)'), '#default_value' => _sitepass_mail_text('approval_body'), '#rows' => 15, '#description' => t('Customize the body of the awaiting approval welcome e-mail, which is sent to new members upon registering.') .' '. t('Available variables are:') .' !username, !site, !passnumber, !uri, !uri_brief, !mailto, !login_uri, !edit_uri, !login_url.');
  
  // Password retreival email is still editted in the orginal location by user.module.
  
  return system_settings_form($form);
}

function _sitepass_mail_text($messageid, $variables = array()) {
  // Check if an admin setting overrides the default string.
  $admin_setting = variable_get('user_mail_' . $messageid, _sitepass_mail_default($messageid,$variables));
  return strtr($admin_setting, $variables);
}


function _sitepass_mail_default($messageid, $variables = array()) {
  
  switch ($messageid) {
    case 'sitepass_subject':
    return t('Site Pass for !username at !site', $variables);
    case 'sitepass_body':
    return t("Dear !username,\n\nSite Pass\n\nNumber: !passnumber\n\nYou may use this number, or the link below to login to !site.\n\nAt !site, instead of having to remember passwords you simply request a Site Pass when you need one. (This is similar to the way that airlines allow you to do on-line check-in with just an Itinerary Number instead of a username/password pair.)\n\nYou may login at !site by  clicking on this link:\n\n!login_url\n\nor copying and pasting it in your browser.\n\nYou may also enter !passnumber into the Site Pass Number field of the User Login area of the site. \n\nThis Site Pass must be used from the location where you requested it, may be used once and expires after one day. Nothing will happen if it's not used.", $variables);
    case 'welcome_subject':
    return t('Account details for !username at !site', $variables);
    case 'welcome_body':
    return t("!username,\n\nThank you for registering at !site. \n\nAt !site, instead of having to remember passwords you simply request a Site Pass when you need one. (This is similar to the way that airlines allow you to do on-line check-in with just an Itinerary Number instead of a username/password pair.)\n\nPlease visit us at !uri\n\n--  !site team", $variables);
    case 'admin_subject':
    return t('An administrator created an account for you at !site', $variables);
    case 'admin_body':
    return t("!username,\n\nA site administrator at !site has created an account for you. \n\nAt !site, instead of having to remember passwords you simply request a Site Pass when you need one. (This is similar to the way that airlines allow you to do on-line check-in with just an Itinerary Number instead of a username/password pair.)\n\nPlease visit us at !uri\n\n--  !site team", $variables);
    case 'approval_subject':
    return t('Account details for !username at !site (pending admin approval)', $variables);
    case 'approval_body':
    return t("!username,\n\nThank you for registering at !site. Your application for an account is currently pending approval. We will be in touch as soon as it is approved. \n\nAt !site, instead of having to remember passwords you simply request a Site Pass when you need one. (This is similar to the way that airlines allow you to do on-line check-in with just an Itinerary Number instead of a username/password pair.)\n\nPlease visit us at !uri\n\n--  !site team", $variables);
  }
}




function sitepass_help($section) {
  switch ($section) {
    case 'admin/help#sitepass':
    $output = t('<p>Site Pass is a module that enables user authentication without passwords.</p><p>When using Site Passes, users who wish to login to your site enter their email address and are then sent a Site Pass Number by email. Users may either enter the Site Pass Number to login or click on the URL in the email.</p><p>A Site Pass is a 6 digit (A-Z0-9) code valid for 24hours, one time only, from a computer at the IP address that requested it. After 3 failed login attempts from one IP address, all unused Site Passes for that IP address will be deleted.</p><p><strong>Setup</strong><br />The <a href="!setupurl">Site Pass Administration page</a> may be used to set the registration emails. Do not show the user\'s password in these emails!</p><p>Site Pass creates two blocks: "Site Pass Request" and "Site Pass User Login". Both these must be enabled.</p><p><strong>Normal Login</strong><br />To make Site Passes the main way that users authenticate to Drupal, disable the normal login block. Normal login for Drupal is still available at admin/user/login. If you want users to be able to request a change in their email address, you will need to enable the contact module and configure it.</p><p><strong>Changes to other forms</strong><br />Site Pass disables editting of registration emails on the "Administer / User Management / User Settings" page and makes them available on the "Administer / Site Passes" page.  Except for user #1, in order to present a consistent user view, Site Pass removes the editting of passwords from user\'s "My Account".  In addition, user\'s ability to edit their own email address has been removed. Once <a href="http://drupal.org/node/85494">emails are properly validated</a> by core this may be reenabled.', array('!setupurl' => url('admin/user/sitepass'), '!loginurl' => url('admin/user/login')));
    return $output;
    break;
    case 'admin/user/sitepass':
    $output = t('<p>Site Passes are an alternative to using passwords for user login. Users visit your site and request a Site Pass which is then delivered by email. Users then login using the Site Pass number. &nbsp;&nbsp;&nbsp;<a href="!url">Help page</a>.</p>', array('!url' => url('admin/help/sitepass')));
    
    return $output;
  }
}


/**
 * Implementation of hook_uninstall().
 */
function sitepass_uninstall() {
  db_query('DROP TABLE {sp_passes}');
}
/**
 * Implementation of hook_perm
 */
function sitepass_perm() {
  return array('Able to download private files');
}

/**
 * Implementation of hook_file_download().
 *
 * Ensure that only authenticated users can access file downloads.
 */
function sitepass_file_download($file) {
  if (!user_access('Able to download private files')) {
    return array(-1);
  }
}

/**
 * Implementation of hook_form_alter()
 *
 */
function sitepass_form_alter($form_id, &$form) {
  global $user;
  global $form_values;
  switch ($form_id) {
    case 'user_admin_settings':
    $form['email']['#description'] = t('* Site Passes Tip ... you may edit the text of Registration\'s Welcome emails by going to <a href="!url">admin/user/sitepass</a>', array('!url' => url('admin/user/sitepass')));
    
    // leave these email details for sitepass's admin form
    unset($form['email']['user_mail_welcome_subject']);
    unset($form['email']['user_mail_welcome_body']);
    unset($form['email']['user_mail_admin_subject']);
    unset($form['email']['user_mail_admin_body']);
    unset($form['email']['user_mail_approval_subject']);
    unset($form['email']['user_mail_approval_body']);
    
    $form['registration']['#description'] .= t('* Site Passes Tip ... You may want to require email confirmation.');
    
    break;
    case 'user_edit':
    // if not admin user #1
    if ($user->uid!=1) {
      unset($form['account']['pass']);
    };
    break;
   
  }
}

/**
 * Implementation of hook_enable().
 */
function sitepass_enable() {
  // save customised welcome emails (so that they can be restored) and set to Site Pass defaults
  variable_set('sitepass_sitepass_mail_welcome_subject',variable_get('user_mail_welcome_subject', FALSE));
  variable_set('user_mail_welcome_subject',_sitepass_mail_default('welcome_subject'));
  
  variable_set('sitepass_sitepass_mail_welcome_body',variable_get('user_mail_welcome_body', FALSE));
  variable_set('user_mail_welcome_body',_sitepass_mail_default('welcome_body'));
  
  variable_set('sitepass_sitepass_mail_admin_subject',variable_get('user_mail_admin_subject', FALSE));
  variable_set('user_mail_admin_subject',_sitepass_mail_default('admin_subject'));
  
  variable_set('sitepass_sitepass_mail_admin_body',variable_get('user_mail_admin_body', FALSE));
  variable_set('user_mail_admin_body',_sitepass_mail_default('admin_body'));
  
  variable_set('sitepass_sitepass_mail_approval_subject',variable_get('user_mail_approval_subject', FALSE));
  variable_set('user_mail_approval_subject',_sitepass_mail_default('approval_subject'));
  
  variable_set('sitepass_sitepass_mail_approval_body',variable_get('user_mail_approval_body', FALSE));
  variable_set('user_mail_approval_body',_sitepass_mail_default('approval_body'));
  
}

/**
 * Implementation of hook_disable().
 */
function sitepass_disable() {
  // restore earlier customised welcome emails.
  variable_set('user_mail_welcome_subject',variable_get('sitepass_sitepass_mail_welcome_subject', FALSE));
  variable_set('user_mail_welcome_body',variable_get('sitepass_sitepass_mail_welcome_body', FALSE));
  variable_set('user_mail_admin_subject',variable_get('sitepass_sitepass_mail_admin_subject', FALSE));
  variable_set('user_mail_admin_body',variable_get('sitepass_sitepass_mail_admin_body', FALSE));
  variable_set('user_mail_approval_subject',variable_get('sitepass_sitepass_mail_approval_subject', FALSE));
  variable_set('user_mail_approval_body',variable_get('sitepass_sitepass_mail_approval_body', FALSE));
  
  
}

/**
 * Convert a decimal number into a string using $base
 * Copied under GPL from http://www.tonymarston.net/php-mysql/showsource.php?file=converter.php
 */
function _dec2string ($decimal, $base)
{
  //DebugBreak();
  global $error;
  $string = null;
  
  $base = (int)$base;
  if ($base < 2 | $base > 36 | $base == 10) {
    echo 'BASE must be in the range 2-9 or 11-36';
    exit;
  } // if
  
  // maximum character string is 36 characters
  $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  
  // strip off excess characters (anything beyond $base)
  $charset = substr($charset, 0, $base);
  
if (!ereg('(^[0-9]{1,16}$)', trim($decimal))) {
    $error['dec_input'] = 'Value must be a positive integer';
    return false;
  } // if
  
  do {
    // get remainder after dividing by BASE
    $remainder = bcmod($decimal, $base);
    
    $char    = substr($charset, $remainder, 1);   // get CHAR from array
    $string  = "$char$string";          // prepend to output
    
    //$decimal   = ($decimal - $remainder) / $base;
    $decimal   = bcdiv(bcsub($decimal, $remainder), $base);
    
  } while ($decimal > 0);
  
  return $string;
  
} // _dec2string

/**
 * Convert a string into a decimal number using $base
 * Copied under GPL from http://www.tonymarston.net/php-mysql/showsource.php?file=converter.php
 */
function _string2dec ($string, $base)
{
  
  global $error;
  $decimal = 0;
  
  $base = (int)$base;
  if ($base < 2 | $base > 36 | $base == 10) {
    echo 'BASE must be in the range 2-9 or 11-36';
    exit;
  } // if
  
  // maximum character string is 36 characters
  $charset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  
  // strip off excess characters (anything beyond $base)
  $charset = substr($charset, 0, $base);
  
  $string = strtoupper(trim($string));
  if (empty($string)) {
    $error[] = 'Input string is empty';
    return false;
  } // if
  
  do {
    $char   = substr($string, 0, 1);  // extract leading character
    $string = substr($string, 1);     // drop leading character
    
    $pos = strpos($charset, $char);   // get offset in $charset
    if ($pos === false) {
      $error[] = "Illegal character ($char) in INPUT string";
      return false;
    } // if
    
    //$decimal = ($decimal * $base) + $pos;
    $decimal = bcadd(bcmul($decimal, $base), $pos);
    
  } while($string <> null);
  
  return $decimal;
  
} 

/**
 * Menu callback; process one request for a sitepass.
 */
function sitepass_send($mail="") {
  global $user;

  // Check if the user is already logged in. The back button is often the culprit here.
  if ($user->uid) {
    drupal_set_message(t('You are already logged in.'));
    drupal_goto();
  }
  else {
	$account = user_load(array('mail' => $mail));
  	if ($account->uid) {
    	$form_values["account"] = $account;
	}
	$form_values["mail"]= $mail;
//drupal_set_message(t('Got here.'));
  //  drupal_goto();
  	drupal_goto(sitepass_request_submit(array(),$form_values));
  }
}


?>

